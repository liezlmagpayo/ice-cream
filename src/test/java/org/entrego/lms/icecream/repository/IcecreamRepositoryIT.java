package org.entrego.lms.icecream.repository;

import static org.junit.Assert.*;

import java.util.List;
import org.entrego.lms.icecream.domain.Icecream;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class IcecreamRepositoryIT {

    @Autowired
    IcecreamRepository icecreamRepository;

    final String ICECREAM_NAME = "Vanilla";
    final String INGREDIENT_NAME = "Milk";
    final String SOURCING_VALUE_NAME = "Non-GMO";

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void findAll() {
        List<Icecream> icecreamList = icecreamRepository.findAll();

        assertEquals(3, icecreamList.size());
    }

    @Test
    public void findOneById() {
        Icecream icecream = icecreamRepository.findOneById(3L).get();

        assertEquals(3L, icecream.getId());
        assertEquals("Cookies and Cream", icecream.getName());
    }

    @Test
    public void findOneByName() {
        Icecream icecream = icecreamRepository.findOneByName(ICECREAM_NAME).get();

        assertEquals(2L, icecream.getId());
    }

    @Test
    public void findByIngredientsName() {
        List<Icecream> icecreams = icecreamRepository.findByIngredientsName(INGREDIENT_NAME);

        assertEquals(3, icecreams.size());
    }

    @Test
    public void findBySourcingValuesName() {
        List<Icecream> icecreams = icecreamRepository.findBySourcingValuesName(SOURCING_VALUE_NAME);

        assertEquals(1, icecreams.size());
    }

}