package org.entrego.lms.icecream.service;

import org.entrego.lms.icecream.domain.Icecream;
import org.entrego.lms.icecream.repository.IcecreamRepository;
import org.entrego.lms.icecream.repository.ListValueRepository;
import org.entrego.lms.icecream.service.dto.IcecreamDTO;
import org.entrego.lms.icecream.service.dto.IcecreamPatchDTO;
import org.entrego.lms.icecream.service.mapper.IcecreamMapper;
import org.entrego.lms.icecream.util.ListSerializer;
import org.entrego.lms.icecream.validation.IcecreamDTOValidator;
import org.entrego.lms.icecream.validation.IcecreamPatchDTOValidator;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.persistence.EntityManager;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

public class IcecreamServiceTest {

    IcecreamService icecreamService;

    @Mock
    IcecreamRepository icecreamRepository;
    @Mock
    ListValueRepository listValueRepository;
    @Mock
    IcecreamDTOValidator icecreamDTOValidator;
    @Mock
    IcecreamPatchDTOValidator icecreamPatchDTOValidator;
    @Mock
    ListSerializer listSerializer;
    @Mock
    IcecreamMapper icecreamMapper;

    private Icecream testIcecream;
    private static final Long ICECREAM_ID = 1L;
    private static final String ICECREAM_NAME = "Chocolate";

    private Icecream testIcecream2;
    private static final Long ICECREAM2_ID = 2L;
    private static final String ICECREAM2_NAME = "Vanilla";

    private IcecreamPatchDTO testPatch;
    private IcecreamDTO testIcecreamDTO;

    private List<Icecream> icecreamList;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        icecreamService = new IcecreamService(
            icecreamRepository,
            icecreamMapper,
            icecreamDTOValidator,
            icecreamPatchDTOValidator,
            listValueRepository,
            listSerializer);

        testIcecream = new Icecream().builder()
                .name(ICECREAM_NAME)
                .id(ICECREAM_ID)
                .build();
        testIcecream2 = new Icecream().builder()
                .name(ICECREAM2_NAME)
                .id(ICECREAM2_ID)
                .build();

        testIcecreamDTO = new IcecreamDTO();
        testIcecreamDTO.setId(ICECREAM_ID);
        testIcecreamDTO.setName(ICECREAM2_NAME);

        testPatch = new IcecreamPatchDTO();
        testPatch.setId(ICECREAM_ID);
        testPatch.setPatchPayload(testIcecreamDTO);

        icecreamList = new ArrayList<>();
        icecreamList.add(testIcecream);
        icecreamList.add(testIcecream2);
    }

    @Test
    public void save() {

        given(icecreamMapper.toEntity(any())).willReturn(testIcecream);
        given(icecreamRepository.save(any())).willReturn(testIcecream);

        Icecream icecream = icecreamService.save(testIcecreamDTO);

        assertEquals(ICECREAM_NAME, icecream.getName());

    }

    @Test
    public void deleteIcecream() {
        given(icecreamRepository.findOneById(anyLong()).get()).willReturn(testIcecream);

        Icecream icecream = icecreamService.deleteIcecream(ICECREAM_ID);

        assertEquals(ICECREAM_NAME, icecream.getName());
    }

    @Test
    public void patchIcecream() {

        given(icecreamRepository.findOneById(anyLong())).willReturn(java.util.Optional.ofNullable(testIcecream));

        Icecream icecream = icecreamService.patchIcecream(testPatch);

        assertEquals(ICECREAM2_NAME, icecream.getName());

    }

    @Test
    public void getAllIcecream() {

        given(icecreamRepository.findAll()).willReturn(icecreamList);

        List<Icecream> myIcecreamList = icecreamService.getAllIcecream();

        assertEquals(2, myIcecreamList.size());
        assertTrue(myIcecreamList.contains(testIcecream2));
    }

    @Test
    public void getIcecream() {
        given(icecreamRepository.findOneById(anyLong())).willReturn(java.util.Optional.ofNullable(testIcecream));

        Icecream icecream = icecreamService.getIcecream(ICECREAM_ID);

        assertEquals(ICECREAM_NAME, icecream.getName());

    }
}