package org.entrego.lms.icecream.controller;

import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.media.sound.SoftTuning;
import java.util.ArrayList;
import java.util.List;
import javax.print.attribute.standard.Media;
import org.entrego.lms.icecream.domain.Icecream;
import org.entrego.lms.icecream.service.IcecreamService;
import org.entrego.lms.icecream.service.dto.IcecreamDTO;
import org.entrego.lms.icecream.service.dto.IcecreamPatchDTO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.json.JsonContent;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.ui.Model;

public class MainControllerTest {

    @Mock
    IcecreamService icecreamService;

    MainController mainController;

    private Icecream testIcecream;
    private static final Long ICECREAM_ID = 1L;
    private static final String ICECREAM_NAME = "Chocolate";

    private Icecream testIcecream2;
    private static final Long ICECREAM2_ID = 2L;
    private static final String ICECREAM2_NAME = "Vanilla";

    private IcecreamPatchDTO testPatch;
    private IcecreamDTO testIcecreamDTO;
    
    private List<Icecream> icecreamList;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        mainController = new MainController(icecreamService);

        testIcecream = new Icecream().builder()
            .name(ICECREAM_NAME)
            .id(ICECREAM_ID)
            .build();
        testIcecream2 = new Icecream().builder()
            .name(ICECREAM2_NAME)
            .id(ICECREAM2_ID)
            .build();

        testPatch = new IcecreamPatchDTO();
        testPatch.setId(ICECREAM_ID);

        testIcecreamDTO = new IcecreamDTO();
        testIcecreamDTO.setId(ICECREAM_ID);

        icecreamList = new ArrayList<>();
        icecreamList.add(testIcecream);
        icecreamList.add(testIcecream2);
    }

    @Test
    public void getAllIcecreams() throws Exception {

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();

        given(icecreamService.getAllIcecream()).willReturn(icecreamList);

        mockMvc.perform(get("/v1/icecream")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(2)))
            .andExpect(jsonPath("$[1].name").value(ICECREAM2_NAME));
        
    }

    @Test
    public void getIcecream() throws Exception {

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();
        given(icecreamService.getIcecream(anyLong())).willReturn(testIcecream);

        mockMvc.perform(get("/v1/icecream/1")
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name").value(ICECREAM_NAME));
    }

    @Test
    public void patchIcecream() throws Exception {

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();
        given(icecreamService.patchIcecream(any())).willReturn(testIcecream);

        ObjectMapper objectMapper = new ObjectMapper();
        String patchString = objectMapper.writeValueAsString(testPatch);

        mockMvc.perform(patch("/v1/icecream/")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .content(patchString))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name").value(ICECREAM_NAME));

    }

    @Test
    public void deleteIcecream() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();
        given(icecreamService.deleteIcecream(anyLong())).willReturn(testIcecream);

        ObjectMapper objectMapper = new ObjectMapper();
        String icecreamDTOString = objectMapper.writeValueAsString(testIcecreamDTO);

        mockMvc.perform(delete("/v1/icecream/1")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name").value(ICECREAM_NAME));
    }

    @Test
    public void addNewIceCream() throws Exception {

        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(mainController).build();
        given(icecreamService.save(any())).willReturn(testIcecream);

        ObjectMapper objectMapper = new ObjectMapper();
        String icecreamDTOString = objectMapper.writeValueAsString(testIcecreamDTO);

        mockMvc.perform(post("/v1/icecream/")
            .accept(MediaType.APPLICATION_JSON)
            .contentType(MediaType.APPLICATION_JSON)
            .content(icecreamDTOString))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.name").value(ICECREAM_NAME));

    }
}