INSERT INTO icecream (id, name, story, image_closed, image_open, allergy_info, dietary_certifications)
VALUES(1L, 'Chocolate', NULL, NULL, NULL, NULL, NULL);
INSERT INTO icecream (id, name, story, image_closed, image_open, allergy_info, dietary_certifications)
VALUES(2L, 'Vanilla', NULL, NULL, NULL, NULL, NULL);
INSERT INTO icecream (id, name, story, image_closed, image_open, allergy_info, dietary_certifications)
VALUES(3L, 'Cookies and Cream', NULL, NULL, NULL, NULL, NULL);
-- insert ingredients and sourcing values
INSERT INTO list_value (id, name)
VALUES(4L, 'Milk');
INSERT INTO list_value (id, name)
VALUES(5L, 'Non-GMO');
-- insert relationship mappings
INSERT INTO icecream_ingredient (icecream_id, ingredient_id)
VALUES(1L, 4L);
INSERT INTO icecream_ingredient (icecream_id, ingredient_id)
VALUES(2L, 4L);
INSERT INTO icecream_ingredient (icecream_id, ingredient_id)
VALUES(3L, 4L);
INSERT INTO icecream_sourcingvalue (icecream_id, sourcing_value_id)
VALUES(1L, 5L);