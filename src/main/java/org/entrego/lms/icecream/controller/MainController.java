package org.entrego.lms.icecream.controller;

import org.entrego.lms.icecream.domain.Icecream;
import org.entrego.lms.icecream.service.IcecreamService;
import org.entrego.lms.icecream.service.dto.IcecreamDTO;
import org.entrego.lms.icecream.service.dto.IcecreamPatchDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v1")
public class MainController {

    private final IcecreamService icecreamService;

    public MainController(IcecreamService icecreamService) {
        this.icecreamService = icecreamService;
    }

    @GetMapping("/icecream")
    public ResponseEntity<List<Icecream>> getAllIcecreams() {
        return  ResponseEntity.ok(icecreamService.getAllIcecream());
    }

    @GetMapping("/icecream/{id}")
    public ResponseEntity<Icecream> getIcecream(@PathVariable Long id) {
        return ResponseEntity.ok(icecreamService.getIcecream(id));
    }

    @GetMapping("/icecream/name={name}")
    public ResponseEntity<Icecream> getIcecream(@PathVariable String name) {
        return ResponseEntity.ok(icecreamService.getIcecreamByName(name));
    }

    @GetMapping("/icecream/ingredient={ingredientName}")
    public ResponseEntity<List<Icecream>> getIcecreamsByIngredient(@PathVariable String ingredientName) {
        return ResponseEntity.ok(icecreamService.getIcecreamsByIngredient(ingredientName));
    }

    @GetMapping("/icecream/sourcingvalue={sourcingValueName}")
    public ResponseEntity<List<Icecream>> getIcecreamsBySourcingValue(@PathVariable String sourcingValueName) {
        return ResponseEntity.ok(icecreamService.getIcecreamsBySourcingValue(sourcingValueName));
    }

    @PatchMapping("/icecream/")
    public ResponseEntity<Icecream> patchIcecream(@RequestBody IcecreamPatchDTO icecreamPatchDTO) {
        return ResponseEntity.ok(icecreamService.patchIcecream(icecreamPatchDTO));
    }

    @DeleteMapping("/icecream/{id}")
    public ResponseEntity<Icecream> deleteIcecream(@PathVariable Long id){
        return ResponseEntity.ok(icecreamService.deleteIcecream(id));
    }

    @PostMapping("/icecream")
    public ResponseEntity<Icecream> addNewIceCream(@RequestBody IcecreamDTO icecreamDTO) {
        return ResponseEntity.ok(icecreamService.save(icecreamDTO));
    }

}
