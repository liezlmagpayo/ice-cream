package org.entrego.lms.icecream.controller.error;

import lombok.experimental.UtilityClass;

import java.net.URI;

@UtilityClass
public class ErrorConstants {

    public static final String PROBLEM_BASE_URL = "https://www.icecream-app.org/api-error";

    public static final URI CONSTRAINT_VIOLATION_TYPE = URI.create(PROBLEM_BASE_URL + "/constraint-violation");
}
