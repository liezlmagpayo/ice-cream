package org.entrego.lms.icecream.controller.error;

import org.zalando.problem.AbstractThrowableProblem;
import org.zalando.problem.Status;

public class ConstraintViolationException extends AbstractThrowableProblem {

    public ConstraintViolationException() {
        super(ErrorConstants.CONSTRAINT_VIOLATION_TYPE, "Invalid input", Status.BAD_REQUEST);
    }

}