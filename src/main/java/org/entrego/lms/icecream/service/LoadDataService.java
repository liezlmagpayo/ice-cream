package org.entrego.lms.icecream.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.entrego.lms.icecream.domain.Icecream;
import org.entrego.lms.icecream.service.dto.IcecreamDTO;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class LoadDataService implements InitializingBean {

    private final IcecreamService icecreamService;

    public LoadDataService(IcecreamService icecreamService) {
        this.icecreamService = icecreamService;
    }

    public void loadInitialData() throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        TypeReference<List<IcecreamDTO>> typeReference = new TypeReference<List<IcecreamDTO>>() {};
        InputStream inputStream = TypeReference.class.getResourceAsStream("/json/icecream.json");

        List<IcecreamDTO> icecreamDTOList = null;
        icecreamDTOList = mapper.readValue(inputStream, typeReference);

        for (IcecreamDTO icecreamDTO : icecreamDTOList) {
            Icecream icecream = icecreamService.save(icecreamDTO);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        loadInitialData();
    }
}
