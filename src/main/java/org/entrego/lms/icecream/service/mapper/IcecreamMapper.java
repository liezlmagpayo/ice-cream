package org.entrego.lms.icecream.service.mapper;

import org.entrego.lms.icecream.domain.Icecream;
import org.entrego.lms.icecream.service.dto.IcecreamDTO;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
@DecoratedWith(IcecreamMapperDecorator.class)
public interface IcecreamMapper {

    @Mapping(target = "ingredients", ignore = true)
    @Mapping(target = "sourcingValues", ignore = true)
    Icecream toEntity(IcecreamDTO dto);

}
