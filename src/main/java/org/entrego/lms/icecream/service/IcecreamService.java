package org.entrego.lms.icecream.service;

import lombok.extern.slf4j.Slf4j;
import org.entrego.lms.icecream.controller.error.ConstraintViolationException;
import org.entrego.lms.icecream.domain.Icecream;
import org.entrego.lms.icecream.domain.ListValue;
import org.entrego.lms.icecream.repository.IcecreamRepository;
import org.entrego.lms.icecream.repository.ListValueRepository;
import org.entrego.lms.icecream.util.ListSerializer;
import org.entrego.lms.icecream.service.dto.IcecreamDTO;
import org.entrego.lms.icecream.service.dto.IcecreamPatchDTO;
import org.entrego.lms.icecream.service.mapper.IcecreamMapper;
import org.entrego.lms.icecream.util.CopyBean;
import org.entrego.lms.icecream.validation.IcecreamDTOValidator;
import org.entrego.lms.icecream.validation.IcecreamPatchDTOValidator;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.stereotype.Service;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.ObjectError;

import javax.transaction.Transactional;
import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Slf4j
@Service
@Transactional
public class IcecreamService {

    private final IcecreamRepository icecreamRepository;
    private final IcecreamDTOValidator icecreamDTOValidator;
    private final IcecreamPatchDTOValidator icecreamPatchDTOValidator;

    private final ListValueRepository listValueRepository;
    private final ListSerializer listSerializer;
    private final IcecreamMapper icecreamMapper;

    public IcecreamService(
            IcecreamRepository icecreamRepository,
            IcecreamMapper icecreamMapper,
            IcecreamDTOValidator icecreamDTOValidator,
            IcecreamPatchDTOValidator icecreamPatchDTOValidator,
            ListValueRepository listValueRepository,
            ListSerializer listSerializer) {
        this.icecreamRepository = icecreamRepository;
        this.icecreamMapper = icecreamMapper;
        this.icecreamDTOValidator = icecreamDTOValidator;
        this.icecreamPatchDTOValidator = icecreamPatchDTOValidator;
        this.listValueRepository = listValueRepository;
        this.listSerializer = listSerializer;
    }

    public Icecream save(IcecreamDTO icecreamDTO) {

        BeanPropertyBindingResult validationResult = new BeanPropertyBindingResult(
                icecreamDTO, "icecreamDTO"
        );

        icecreamDTOValidator.validate(icecreamDTO, validationResult);
        if (validationResult.hasErrors()) {
            ObjectError objectError = validationResult.getAllErrors().get(0);
            throw new ConstraintViolationException();
        }

        Icecream savedIcecream = icecreamMapper.toEntity(icecreamDTO);

        icecreamRepository.save(savedIcecream);

        return savedIcecream;
    }

    public Icecream deleteIcecream(Long id){
        Icecream deletedIcecream = icecreamRepository.findOneById(id).get();
        icecreamRepository.delete(deletedIcecream);

        return deletedIcecream;
    }

    public Icecream patchIcecream(IcecreamPatchDTO icecreamPatchDTO) {

        BeanPropertyBindingResult validationResult = new BeanPropertyBindingResult(
                icecreamPatchDTO, "icecreamPatchDTO"
        );

        icecreamPatchDTOValidator.validate(icecreamPatchDTO, validationResult);
        if (validationResult.hasErrors()) {
            ObjectError objectError = validationResult.getAllErrors().get(0);
            throw new ConstraintViolationException();
        }

        Icecream icecream = icecreamRepository.findOneById(icecreamPatchDTO.getId()).get();
        IcecreamDTO patch = icecreamPatchDTO.getPatchPayload();
        patch.setId(icecreamPatchDTO.getId());
        // --- START
        /**
         * Can you explain to me the process starting to up
         * to the "---END"? :)
         */
        CopyBean beanUtils = new CopyBean();
        beanUtils.copyNonNullProperties(patch, icecream);

        // beanUtils cant copy list fields so need to handle that
        final BeanWrapper patchBean = new BeanWrapperImpl(patch);
        final BeanWrapper icecreamBean = new BeanWrapperImpl(icecream);

        PropertyDescriptor[] propertyDescriptors = patchBean.getPropertyDescriptors();
        for ( PropertyDescriptor property: propertyDescriptors ) {
            String propertyName = property.getName();
            if( patchBean.getPropertyValue(propertyName) != null) {
                Class propertyClass = patchBean.getPropertyValue(propertyName).getClass();
                if( propertyClass == ArrayList.class) {
                    List<String> stringList =
                            (List<String>) patchBean.getPropertyValue(propertyName);

                    Set<ListValue> listValueSet = listSerializer.deserialize(
                            stringList,
                            listValueRepository);

                    icecreamBean.setPropertyValue(propertyName, listValueSet);
                }
            }
        }
        // --- END
        return icecream;
    }

    public List<Icecream> getAllIcecream() {

        return icecreamRepository.findAll();
    }

    public Icecream getIcecream(Long id) {

        return icecreamRepository.findOneById(id).get();
    }

    public Icecream getIcecreamByName(String name) {

        Icecream icecream = icecreamRepository.findOneByName(name).get();

        return icecream;
    }

    public List<Icecream> getIcecreamsByIngredient(String ingredientName) {

        List<Icecream> icecreams = icecreamRepository.findByIngredientsName(ingredientName);

        return icecreams;
    }

    public List<Icecream> getIcecreamsBySourcingValue(String sourcingValueName) {

        List<Icecream> icecreams = icecreamRepository.findBySourcingValuesName(sourcingValueName);

        return icecreams;
    }
}