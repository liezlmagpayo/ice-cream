package org.entrego.lms.icecream.service.mapper;

import org.entrego.lms.icecream.domain.Icecream;
import org.entrego.lms.icecream.domain.ListValue;
import org.entrego.lms.icecream.repository.ListValueRepository;
import org.entrego.lms.icecream.util.ListSerializer;
import org.entrego.lms.icecream.service.dto.IcecreamDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Set;

public abstract class IcecreamMapperDecorator implements IcecreamMapper {

    @Autowired
//    @Qualifier("delegate")
    private IcecreamMapper delegate;

    @Autowired
    private ListValueRepository listValueRepository;

    @Override
    public Icecream toEntity(IcecreamDTO dto) {

        Icecream icecream = delegate.toEntity(dto);

        ListSerializer listSerializer = new ListSerializer();
        Set<ListValue> ingredients = listSerializer.deserialize(dto.getIngredients(), listValueRepository);
        Set<ListValue> sourcingValues = listSerializer.deserialize(dto.getSourcingValues(), listValueRepository);
        icecream.setIngredients(ingredients);
        icecream.setSourcingValues(sourcingValues);

        return icecream;
    }

}
