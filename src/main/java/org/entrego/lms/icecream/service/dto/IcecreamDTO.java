package org.entrego.lms.icecream.service.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class IcecreamDTO implements Serializable {

    @JsonProperty("productId")
    private long id;
    private String name;
    private String description;
    private String story;
    @JsonProperty("image_closed")
    private String imageClosed;
    @JsonProperty("image_open")
    private String imageOpen;
    @JsonProperty("allergy_info")
    private String allergyInfo;
    @JsonProperty("dietary_certifications")
    private String dietaryCertifications;

    private List<String> ingredients;
    @JsonProperty("sourcing_values")
    private List<String> sourcingValues;

}