package org.entrego.lms.icecream.service.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class IcecreamPatchDTO implements Serializable {

    private Long id;

    private IcecreamDTO patchPayload;
}
