package org.entrego.lms.icecream.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import org.entrego.lms.icecream.domain.ListValue;
import org.entrego.lms.icecream.repository.ListValueRepository;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Component
public class ListSerializer extends StdSerializer<Set<ListValue>> {

    public ListSerializer() {
        this(null);
    }

    public ListSerializer(Class<Set<ListValue>> t) {
        super(t);
    }

    @Override
    public void serialize(
            Set<ListValue> listValues, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {

        jgen.writeStartArray();
        for (ListValue listValue : listValues) {
            jgen.writeString(listValue.getName());
        }
        jgen.writeEndArray();
    }

    public Set<ListValue> deserialize(List<String> stringList, ListValueRepository repository) {

        Set<ListValue> listValueSet = new HashSet<>();
        for ( String name: stringList ) {
            ListValue listValue = repository.findOneByName(name);
            if(listValue == null){
                ListValue newListValue = new ListValue().builder()
                        .name(name)
                        .build();
                repository.save(newListValue);
                listValueSet.add(newListValue);
            }
            else {
                listValueSet.add(listValue);
            }
        }
        return listValueSet;
    }
}