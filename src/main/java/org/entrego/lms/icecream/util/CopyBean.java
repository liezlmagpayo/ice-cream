package org.entrego.lms.icecream.util;

import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.PropertyDescriptor;
import java.util.HashSet;
import java.util.Set;

/**
 * What is the use of this class?
 */
@NoArgsConstructor
public class CopyBean {

    public void copyNonNullProperties(Object src, Object dest) {
        BeanUtils.copyProperties(src, dest, ignoreTheseNullProperties(src));
    }

    private String[] ignoreTheseNullProperties ( Object source ) {
        final BeanWrapper beanWrapper = new BeanWrapperImpl(source);
        PropertyDescriptor[] propertyDescriptors = beanWrapper.getPropertyDescriptors();

        Set<String> nullFields = new HashSet<String>();
        for(PropertyDescriptor property : propertyDescriptors) {
            Object value = beanWrapper.getPropertyValue(property.getName());
            if (value == null) {
                nullFields.add(property.getName());
            }
        }
        String[] result = new String[nullFields.size()];
        return nullFields.toArray(result);
    }

}