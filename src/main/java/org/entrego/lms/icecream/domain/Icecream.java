package org.entrego.lms.icecream.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.entrego.lms.icecream.util.ListSerializer;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Icecream {

    @Id
    private long id;

    @Column(nullable=false)
    private String name;
    private String description;

    @Lob
    private String story;
    private String imageClosed;
    private String imageOpen;
    private String allergyInfo;
    private String dietaryCertifications;

    @ManyToMany(fetch = FetchType.EAGER)
    @JsonSerialize(using = ListSerializer.class)
    @JoinTable(name = "icecream_sourcingvalue",
            joinColumns = {@JoinColumn(name = "icecream_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "sourcing_value_id", referencedColumnName = "id")})
    private Set<ListValue> sourcingValues = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER)
    @JsonSerialize(using = ListSerializer.class)
    @JoinTable(name = "icecream_ingredient",
            joinColumns = {@JoinColumn(name = "icecream_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "ingredient_id", referencedColumnName = "id")})
    private Set<ListValue> ingredients = new HashSet<>();

}