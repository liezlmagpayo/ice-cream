package org.entrego.lms.icecream.validation;

import org.entrego.lms.icecream.domain.Icecream;
import org.entrego.lms.icecream.repository.IcecreamRepository;
import org.entrego.lms.icecream.service.dto.IcecreamDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public class IcecreamDTOValidator implements Validator {

    private static final List<String> contentTypes = Arrays.asList(".png", ".jpeg", ".jpg");
    private final IcecreamRepository icecreamRepository;

    public IcecreamDTOValidator(IcecreamRepository icecreamRepository) {
        this.icecreamRepository = icecreamRepository;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return IcecreamDTO.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        IcecreamDTO icecreamDTO = (IcecreamDTO) o;

        Optional<Icecream> optionalIcecream = icecreamRepository.findOneById(icecreamDTO.getId());

        if (optionalIcecream.isPresent()) {
            errors.reject(
                    "icecream_already_exists", "Invalid product Id"
            );
            return;
        }

        /**
         * What is the purpose of this two?
         */
        Optional<String> optionalImageOpen = Optional.ofNullable(icecreamDTO.getImageOpen());
        ExtensionValidator(errors, optionalImageOpen, contentTypes);

        Optional<String> optionalImageClosed = Optional.ofNullable(icecreamDTO.getImageClosed());
        ExtensionValidator(errors, optionalImageClosed, contentTypes);
    }

    /**
     * What is the purpose of this function?
     */
    static void ExtensionValidator(Errors errors, Optional<String> optionalFile, List<String> contentTypes) {
        if(optionalFile.isPresent()) {
            String file = optionalFile.get();
            String extension = file.substring(file.lastIndexOf("."));
            System.out.println(extension);
            if(!contentTypes.contains(extension)){
                errors.rejectValue("patchPayload", "icecreamPatchDTO.patchPayload.invalid");
            }
        }
    }
}
