package org.entrego.lms.icecream.validation;

import org.entrego.lms.icecream.domain.Icecream;
import org.entrego.lms.icecream.repository.IcecreamRepository;
import org.entrego.lms.icecream.service.dto.IcecreamPatchDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Component
public class IcecreamPatchDTOValidator implements Validator {

    private static final List<String> contentTypes = Arrays.asList(".png", ".jpeg", ".jpg");
    private final IcecreamRepository icecreamRepository;

    public IcecreamPatchDTOValidator(IcecreamRepository icecreamRepository) {
        this.icecreamRepository = icecreamRepository;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return IcecreamPatchDTO.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        IcecreamPatchDTO icecreamPatchDTO = (IcecreamPatchDTO) o;

        Optional<Icecream> optionalIcecream = icecreamRepository.findOneById(icecreamPatchDTO.getId());

        if (!optionalIcecream.isPresent()) {
            errors.reject(
                    "icecream_does_not_exist", "Invalid product Id"
            );
            return;
        }

        /**
         * What is the purpose of this two?
         */
        Optional<String> optionalImageOpen = Optional.ofNullable(icecreamPatchDTO.getPatchPayload().getImageOpen());
        IcecreamDTOValidator.ExtensionValidator(errors, optionalImageOpen, contentTypes);

        Optional<String> optionalImageClosed = Optional.ofNullable(icecreamPatchDTO.getPatchPayload().getImageClosed());
        IcecreamDTOValidator.ExtensionValidator(errors, optionalImageClosed, contentTypes);

    }
}
