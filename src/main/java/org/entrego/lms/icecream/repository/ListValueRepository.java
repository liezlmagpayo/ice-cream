package org.entrego.lms.icecream.repository;

import org.entrego.lms.icecream.domain.ListValue;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * What is the use of this?
 */
@Repository
public interface ListValueRepository extends CrudRepository<ListValue, Long> {

    public ListValue findOneByName(String name);

}