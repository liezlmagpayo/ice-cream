package org.entrego.lms.icecream.repository;

import org.entrego.lms.icecream.domain.Icecream;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Do you know that JpaRepository has an existing findAll() and findOne() methods? :)
 */
@Repository
@SuppressWarnings("unused")
public interface IcecreamRepository extends JpaRepository<Icecream, Long> {

    Optional<Icecream> findOneById(Long id);
    Optional<Icecream> findOneByName(String name);
    List<Icecream> findAll();
    List<Icecream> findByIngredientsName(String name);
    List<Icecream> findBySourcingValuesName(String name);

}
