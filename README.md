# Icecream App

## Setup

- `git clone git@gitlab.com:liezlmagpayo/ice-cream.git`
- start the app with `mvn spring-boot:run`

## Populating the Database

- the app will automatically populate the database with data from /resources/json/icecream.json file at start up

## HTTP verbs

- GET `/v1/icecream/`
    - Gets a list of all icecream flavors
- GET `/v1/icecream/{id}/`
    - Gets the icecream flavor with the corresponding product id
- POST `/v1/icecream/`
    - Saves an icecream object. Refer to the JSON file in the resources folder for the JSON structure
- PATCH `/v1/icecream/patch/`
    - Updates specified fields of an existing flavor instance denoted by the productId

```
{
    "productId" : 2168,
    "patchPayload" : {
        "story" : "new value here"
    }
}
```
- DELETE `/v1/icecream/{id}/`
    - Deletes the icecream flavor with the specified product id

## Architecture

 I started out with 3 tables: ICECREAM, which contains all the information except for values that are lists.
 These were separated to INGREDIENT and SOURCING_VALUE tables with their own many to many mappings to the ICECREAM table.
 However, I tried to find a way to make these list values more generic so that there can be one method to serialize
 and deserialize these values. This led to combining these list values into a single LIST_VALUE table which will
 enable the app to add or remove more keys with this specific list format. This specific structure simply favors
 non-redundancy of the code which will convert from DTO to Entity and vice-versa versus a more organized internal data
 table structure.